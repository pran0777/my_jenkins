provider "aws" {
  region                   = var.region
  shared_config_files      = ["/root/.aws/config"]
  shared_credentials_files = ["/root/.aws/credentials"]
}
resource "aws_vpc" "myvpc" {

  cidr_block       = var.cidr_block
  instance_tenancy = var.instance_tenancy
  tags             = var.tags
}
variable "region" {
  type        = string
  description = "region variables"
  default     = "us-east-1"
}
variable "cidr_block" {
  type        = string
  description = "cidr variables"
  default     = "192.168.0.0/16"
}
variable "instance_tenancy" {
  type        = string
  description = "instance tenancy"
  default     = "default"
}
variable "tags" {
  type        = map(any)
  description = "default"
  default = {
    Name = "myvpc_in_us-east-2"
  }
}